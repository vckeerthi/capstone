package com.example.capstone1;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;


public class moleActivity extends AppCompatActivity {
    WhackaMole mole;

//    FrameLayout game;
//    RelativeLayout gamebuttons;
//
//    Button weaponsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        mole = new WhackaMole(this);
//        game = new FrameLayout(this);
//        gamebuttons = new RelativeLayout(this);
//        weaponsButton = new Button(this);
//        weaponsButton.setText("shoot");
//
//        RelativeLayout.LayoutParams button = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
//        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
//        gamebuttons.setLayoutParams(params);
//        gamebuttons.addView(weaponsButton);
//
//        button.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
//        button.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
//        weaponsButton.setLayoutParams(button);
//
//
//
//        game.addView(mole);
//        game.addView(gamebuttons);
//        setContentView(game);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        weaponsButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mole.spawnPlayerBullet();
//            }
//        });
        Display display = getWindowManager().getDefaultDisplay();
        Point size  = new Point();
        display.getSize(size);

        mole = new WhackaMole(this, size.x, size.y);

        // Make GameEngine the view of the Activity
        setContentView(mole);

    }
    @Override
    protected void onResume() {
        super.onResume();
        mole.startGame();
    }


    @Override
    protected void onPause() {
        super.onPause();
        mole.pauseGame();
    }
}
