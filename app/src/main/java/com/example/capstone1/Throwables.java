package com.example.capstone1;

import android.content.Context;
import android.content.Intent;
import android.gesture.GestureStroke;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.ImageButton;
import android.widget.Scroller;

public class Throwables extends SurfaceView implements Runnable {
    private final SurfaceHolder holder;
    private final Paint paintbrush;
    int screenHeight;
    int screenWidth;
    String message;
    Enemy enemy;
    Scroller scroller;
    Weapon weapon;
    private Canvas canvas;
    int life = 10;
    int score = 0;
    Thread gameThread;
    Boolean gameIsRunning;

    public Throwables(Context context, int w, int h){


        super(context);
        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;
        message = StaticClass.messageClass.message;

        enemy = new Enemy(getContext(), 1800, 200);
        weapon = new Weapon(getContext(), 100, 200, message);


    }


    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            this.canvas.drawColor(Color.argb(255, 255, 255, 255));
            paintbrush.setColor(Color.BLACK);

            canvas.drawBitmap(Bitmap.createScaledBitmap(
                    this.enemy.getEnemyImage(),350, 350, false),this.enemy.getxPos(), this.enemy.getyPos(), paintbrush);
            canvas.drawBitmap(this.weapon.getWeaponImage(), this.weapon.getxPos(), this.weapon.getyPos(), paintbrush);

            paintbrush.setStyle(Paint.Style.FILL);
            paintbrush.setTextSize(55);
            canvas.drawText("Lifes Left: " + this.life,this.screenWidth-700,120,paintbrush);
            canvas.drawText("Score: " + this.score,600,120,paintbrush);
            for (int i = 0; i < this.weapon.getBullets().size(); i++)
            {
                Rect bullet = this.weapon.getBullets().get(i);
                canvas.drawRect(bullet, paintbrush);

            }
            this.holder.unlockCanvasAndPost(canvas);

        }
    }
    private void updatePositions() {
        if (this.enemy.getEnemyHitbox().intersect(weapon.getWeaponHitbox())) {

            this.enemy.setGotHit(this.enemy.getGotHit() + 1);
            this.life = life -1;
            this.score = StaticClass.messageClass.score2 + 10;
            if(life <= 0){
                this.gamefinished();
            }

        }
    }

    private void gamefinished() {
        Intent i = new Intent(this.getContext(),LevelUp.class);
        this.getContext().startActivity(i);
    }

    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }

    }



    public void setFPS() {
        try {

            gameThread.sleep(60);
        } catch (Exception e) {

        }
    }

    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {

        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?
        if (userAction == MotionEvent.ACTION_DOWN)
        {


            int middleOfScreen = this.screenWidth/ 2;
            if (event.getX() <= middleOfScreen)
            {
                Intent i = new Intent(this.getContext(),weaponsActivity.class);
                this.getContext().startActivity(i);
            }
            else if (event.getX() > middleOfScreen)
            {
//                Scroller scroller;
//                weapon.getWeaponImage().
//
//                fling(weapon.getxPos(), weapon.getyPos(), 350 , 20, weapon.getxPos(),  weapon.getyPos(), enemy.getxPos(), enemy.getyPos());
//                postInvalidate();
            }
        }
        else if (userAction == MotionEvent.ACTION_UP)
        {
        }
        return true;
    }

}
