package com.example.capstone1;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.capstone1.Helper.BitmapHelper;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.os.Environment.getExternalStoragePublicDirectory;

public class MainActivity extends AppCompatActivity {
    Button btntakepic;
    ImageView pic;
    File photoFile;
    String pathToFile;
    Intent takepic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btntakepic = findViewById(R.id.takepicture);
        if(Build.VERSION.SDK_INT >= 23){
            requestPermissions(new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE},2);
        }
        pic = findViewById(R.id.image);
    }
    public void onclickbtn(View view){
        takepicture();
    }
    public void nextclick(View view){
        Intent i = new Intent(MainActivity.this,animate.class);
        startActivity(i);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if(Math.abs(resultCode) == 1){


                Bitmap bitmap = BitmapFactory.decodeFile(pathToFile);
                int width = bitmap.getWidth();
                int height = bitmap.getHeight();
                int newWidth = 200;
                int newHeight = 200;
                float scaleWidth = ((float) newWidth) / width;
                float scaleHeight = ((float) newHeight) / height;
                Matrix m = new Matrix();
                m.postScale(scaleWidth, scaleHeight);
                m.postRotate(90);
                Bitmap resized = Bitmap.createBitmap(bitmap,0,0,width,height,m,true);
//                Bitmap resize = Bitmap.createScaledBitmap(resized,200,189,true);
                pic.setImageBitmap(resized);
                BitmapHelper.getInstance().setBitmap(resized);
            }
        }
    }


    private void takepicture() {
         takepic = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(takepic.resolveActivity(getPackageManager()) != null){
            photoFile = null;
            photoFile = createPhotoFile();
            if(photoFile != null){
                pathToFile = photoFile.getAbsolutePath();
                Uri photoURI = FileProvider.getUriForFile(MainActivity.this,"com.example.capstone1.fileprovider",photoFile);
                takepic.putExtra(MediaStore.EXTRA_OUTPUT,photoURI);
                startActivityForResult(takepic,1);
            }
        }
    }

    private File createPhotoFile() {
        String name = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File storageDir = getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
             image = File.createTempFile(name,".jpg",storageDir);
        } catch (IOException e) {
            Log.d("mylog", "Excep:"+ e.toString());
        }
        return image;

    }
}
