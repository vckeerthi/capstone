package com.example.capstone1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.dynamicanimation.animation.DynamicAnimation;
import androidx.dynamicanimation.animation.FlingAnimation;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Debug;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class animate extends AppCompatActivity {
//    Throwables throwables;
Bitmap enemybit;
    Bitmap itembit;
    Enemy enemy ;
    Weapon weapon;
    String message;
    ImageView pic;
    ImageView itempic;

    Weapon black;
    TextView healthView;
    TextView scoreView;
    int x;
    int y;
    int picx;
    int picy;
    int [] posXY;
    int [] weapXY;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_animate);
//        Display display = getWindowManager().getDefaultDisplay();
//        Point size  = new Point();
//        display.getSize(size);
//
//        throwables = new Throwables(this, size.x, size.y);
//        setContentView(throwables);
        message = StaticClass.messageClass.message;
        enemy= new Enemy(getApplicationContext(), 1800, 200);
        weapon = new Weapon(getApplicationContext(),100,200,message);
        black = new Weapon(getApplicationContext(),100,200,"black");
        enemybit = enemy.getEnemyImage();
        itembit = weapon.getWeaponImage();
        healthView = findViewById(R.id.texts);
        healthView.setText("health:"+ StaticClass.messageClass.health);
        scoreView = findViewById(R.id.score);
        scoreView.setText("score:"+ StaticClass.messageClass.score1);
        pic = findViewById(R.id.weapon);
        pic.setImageBitmap(enemybit);
        itempic =  findViewById(R.id.w);
        itempic.setImageBitmap(itembit);
        Button select = findViewById(R.id.select);
        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(animate.this, weaponsActivity.class);
                startActivity(i);
            }
        });

        Button throwit = findViewById(R.id.throwobj);

        throwit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v ) {
                if (message != "black") {
                    FlingAnimation flingAnimation = new FlingAnimation(itempic, DynamicAnimation.X);
                    flingAnimation.setStartVelocity(3800f);
                    flingAnimation.setFriction(0.5f);
                    flingAnimation.start();
                    posXY = new int[2];
                    itempic.getLocationOnScreen(posXY);
                    x = posXY[0];
                    y = posXY[1];
                    weapXY = new int[2];
                    pic.getLocationOnScreen(weapXY);
                    picx = weapXY[0];
                    picy = weapXY[1];
                    Log.d("x",String.valueOf(x));
                    Log.d("y",String.valueOf(y));

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            itempic.getLocationOnScreen(posXY);
                            x = posXY[0];
                            y = posXY[1];
                            Log.d("x",String.valueOf(x));
                            Log.d("y",String.valueOf(y));
                            Log.d("picx",String.valueOf(picx));
                            Log.d("picy",String.valueOf(picy));
                            if(StaticClass.messageClass.health <= 0) {
                                Intent intent = new Intent(animate.this,LevelUp.class);
                                startActivity(intent);
                                StaticClass.messageClass.health = 100;

                            }
                            else{
                                if (x >= picx - 150) {
                                    if (message.contains("egg")) {
                                        StaticClass.messageClass.health = StaticClass.messageClass.health - 20;
                                        StaticClass.messageClass.score1 = StaticClass.messageClass.score1 + 20;
                                        healthView.setText("health:" +StaticClass.messageClass.health);
                                        scoreView.setText("score:"+StaticClass.messageClass.score1);
                                        FlingAnimation flingAnimation = new FlingAnimation(itempic, DynamicAnimation.X);
                                        flingAnimation.setStartVelocity(-3400f);
                                        flingAnimation.setFriction(0.5f);
                                        flingAnimation.start();
                                        if(StaticClass.messageClass.health <= 0){
                                            Intent intent = new Intent(animate.this,LevelUp.class);
                                            startActivity(intent);
                                            StaticClass.messageClass.health = 100;
                                        }

                                    } else if (message.contains("tomato")) {
                                        StaticClass.messageClass.health = StaticClass.messageClass.health - 10;
                                        StaticClass.messageClass.score1 = StaticClass.messageClass.score1 + 10;
                                        healthView.setText("health:" + StaticClass.messageClass.health);
                                        scoreView.setText("score:"+StaticClass.messageClass.score1);
                                        FlingAnimation flingAnimation = new FlingAnimation(itempic, DynamicAnimation.X);
                                        flingAnimation.setStartVelocity(-3400f);
                                        flingAnimation.setFriction(0.5f);
                                        flingAnimation.start();
                                        if(StaticClass.messageClass.health <= 0){
                                            Intent intent = new Intent(animate.this,LevelUp.class);
                                            startActivity(intent);
                                            StaticClass.messageClass.health = 100;
                                        }

                                    } else if (message.contains("stone")) {

                                        StaticClass.messageClass.health = StaticClass.messageClass.health - 30;
                                        StaticClass.messageClass.score1 = StaticClass.messageClass.score1 + 30;
                                        healthView.setText("health:" + StaticClass.messageClass.health);
                                        scoreView.setText("score:"+StaticClass.messageClass.score1);
                                        FlingAnimation flingAnimation = new FlingAnimation(itempic, DynamicAnimation.X);
                                        flingAnimation.setStartVelocity(-3400f);
                                        flingAnimation.setFriction(0.5f);
                                        flingAnimation.start();
//                                        itempic.setImageBitmap(black.getWeaponImage());
                                        if(StaticClass.messageClass.health <= 0){
                                            Intent intent = new Intent(animate.this,LevelUp.class);
                                            startActivity(intent);
                                            StaticClass.messageClass.health = 100;
                                        }

                                    }
                                }

                            }

                        }
                    }, 1000);

                }
                else {
                    Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


}
//    @Override
//    protected void onResume() {
//        super.onResume();
//        throwables.startGame();
//    }
//
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        throwables.pauseGame();
//    }



