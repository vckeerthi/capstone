package com.example.capstone1;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import java.util.ArrayList;

public class Weapon {
    private Bitmap weaponImage;
    private Rect weaponHitbox;
    public int gotHit;
    private Bitmap resized;
    private int speed = 20;


    private int xPos;
    private int yPos;

    private ArrayList<Rect> Bullets = new ArrayList<Rect>();

    public Weapon(Context context, int xPosition, int yPosition, String weapon)
    {
        this.xPos = xPosition;
        this.yPos = yPosition;
        this.gotHit = 0;

        if (weapon == "egg")
        {
            this.weaponImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.egg);
            resized = Bitmap.createScaledBitmap(
                    weaponImage,200, 200, false);
        }
        else if (weapon == "tomato")
        {
            this.weaponImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.tomato);
            resized = Bitmap.createScaledBitmap(
                    weaponImage,200, 200, false);
        }
        else if (weapon == "stone")
        {
            this.weaponImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.stone);
            resized = Bitmap.createScaledBitmap(
                    weaponImage,200, 200, false);

        }
        else if (weapon == "black")
        {
            this.weaponImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.black);
            resized = Bitmap.createScaledBitmap(
                    weaponImage,200, 200, false);

        }
        else if (weapon == "hammer")
        {
            this.weaponImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.hammer);

            resized = Bitmap.createScaledBitmap(
                    weaponImage,200, 200, false);

        }
        else if (weapon == "gun")
        {
            this.weaponImage = BitmapFactory.decodeResource(context.getResources(), R.drawable.gun);
            resized = Bitmap.createScaledBitmap(
                    weaponImage,250, 250, false);

        }
        this.weaponHitbox = new Rect(
                this.xPos,
                this.yPos,
                this.xPos + this.resized.getWidth(),
                this.yPos + this.resized.getHeight()
        );


    }

    public Bitmap getWeaponImage() {
        return resized;
    }

    public void setWeaponImage(Bitmap weaponImage) {
        this.resized = weaponImage;
    }

    public Rect getWeaponHitbox() {
        return weaponHitbox;
    }

    public void setEnemyHitbox(Rect enemyHitbox) {
        this.weaponHitbox = enemyHitbox;
    }

    public int getGotHit() {
        return gotHit;
    }

    public void setGotHit(int gotHit) {
        this.gotHit = gotHit;
    }

    public int getxPos() {
        return xPos;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public int getyPos() {
        return yPos;
    }

    public void setyPos(int yPos) {
        this.yPos = yPos;
    }


    public void updateHitbox() {
        this.weaponHitbox.left = this.xPos;
        this.weaponHitbox.top = this.yPos;
        this.weaponHitbox.right = this.xPos + this.resized.getWidth();
        this.weaponHitbox.bottom = this.yPos + this.resized.getHeight();
    }

    public ArrayList<Rect> getBullets() {
        return Bullets;
    }

    public void setBullets(ArrayList<Rect> playerBullets) {
        this.Bullets = playerBullets;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
}



