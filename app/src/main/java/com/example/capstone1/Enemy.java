package com.example.capstone1;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;

import com.example.capstone1.Helper.BitmapHelper;

import static android.os.Environment.getExternalStoragePublicDirectory;
import java.io.File;

public class Enemy {
    private Bitmap enemyImage;
    private Rect enemyHitbox;
    private Bitmap resized;
    public int gotHit;



    private int xPos;
    private int yPos;



    public Enemy(Context context, int xPosition, int yPosition)
    {
        this.xPos = xPosition;
        this.yPos = yPosition;
        this.gotHit = 0;
        Matrix matrix = new Matrix();
        matrix.postRotate(90);
        matrix.postScale(250,250);


      this.enemyImage = BitmapHelper.getInstance().getBitmap();

//        Bitmap rotate = Bitmap.createBitmap(enemyImage,getxPos(),getyPos(),250,250,matrix,false);
        resized = Bitmap.createScaledBitmap(enemyImage,150, 150, false);
        this.enemyHitbox = new Rect(
                this.xPos,
                this.yPos,
                this.xPos + this.resized.getWidth(),
                this.yPos + this.resized.getHeight()
        );
    }

    public Bitmap getEnemyImage() {
        return resized;
    }

    public void setEnemyImage(Bitmap enemyImage) {
        this.resized = enemyImage;
    }

    public Rect getEnemyHitbox() {
        return enemyHitbox;
    }

    public void setEnemyHitbox(Rect enemyHitbox) {
        this.enemyHitbox = enemyHitbox;
    }

    public int getxPos() {
        return xPos;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public int getyPos() {
        return yPos;
    }

    public void setyPos(int yPos) {
        this.yPos = yPos;
    }

    public int getGotHit() {
        return gotHit;
    }

    public void setGotHit(int gotHit) {
        this.gotHit = gotHit;
    }


    public void updateHitbox() {
        this.enemyHitbox.left = this.xPos;
        this.enemyHitbox.top = this.yPos;
        this.enemyHitbox.right = this.xPos + this.resized.getWidth();
        this.enemyHitbox.bottom = this.yPos + this.resized.getHeight();
    }
}



