package com.example.capstone1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class weaponsActivity extends AppCompatActivity {
    ImageButton tomato;
    ImageButton egg;
    ImageButton stone;
    String message;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weapons);

        tomato = findViewById(R.id.tomato);
        egg = findViewById(R.id.egg);
        stone = findViewById(R.id.stone);

        tomato.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(weaponsActivity.this,animate.class);
                message = "tomato";
                StaticClass.messageClass.message = message;
                startActivity(intent);
            }
        });
        egg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(weaponsActivity.this,animate.class);
                message = "egg";
                StaticClass.messageClass.message = message;
                startActivity(intent);
            }
        });
        stone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(weaponsActivity.this,animate.class);
                message = "stone";
                StaticClass.messageClass.message = message;
                startActivity(intent);
            }
        });




    }
}
