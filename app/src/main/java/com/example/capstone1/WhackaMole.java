package com.example.capstone1;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Random;

public class WhackaMole extends SurfaceView implements Runnable {
    int screenHeight;
    int screenWidth;

    int enemyspeed = 50;
    int enemyMoveTime = 20;

    boolean gameIsRunning;

    Thread gameThread;
    SurfaceHolder holder;
    Canvas canvas;
    Paint paintbrush;

    Enemy enemy;
    Weapon weapon;

    int score = StaticClass.messageClass.score2;
    int life = 10;

    public WhackaMole(Context context,int w, int h){

        super(context);

        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        this.screenWidth = w;
        this.screenHeight = h;

        enemy = new Enemy(getContext(), 1800, 200);
        weapon = new Weapon(getContext(), 100, 200, "gun");


    }


    public void redrawSprites() {
        if (this.holder.getSurface().isValid()) {
            this.canvas = this.holder.lockCanvas();

            this.canvas.drawColor(Color.argb(255, 255, 255, 255));
            paintbrush.setColor(Color.BLACK);

            canvas.drawBitmap(Bitmap.createScaledBitmap(
                    this.enemy.getEnemyImage(),350, 350, false),this.enemy.getxPos(), this.enemy.getyPos(), paintbrush);
            canvas.drawBitmap(this.weapon.getWeaponImage(), this.weapon.getxPos(), this.weapon.getyPos(), paintbrush);

            paintbrush.setStyle(Paint.Style.FILL);
            paintbrush.setTextSize(55);
            canvas.drawText("Lifes Left: " + this.life,this.screenWidth-700,120,paintbrush);
            canvas.drawText("Score: " + this.score,600,120,paintbrush);
            for (int i = 0; i < this.weapon.getBullets().size(); i++)
            {
                Rect bullet = this.weapon.getBullets().get(i);
                canvas.drawRect(bullet, paintbrush);

            }
            this.holder.unlockCanvasAndPost(canvas);

        }
    }

    int loop = 0;

    public void updatePositions() {
        loop = loop + 1;

        int bulletSpeed= 20;
        for (int i = 0; i < this.weapon.getBullets().size(); i++) {
            Rect bullet = this.weapon.getBullets().get(i);
            bullet.left = bullet.left + bulletSpeed;
            bullet.right = bullet.right + bulletSpeed;
        }
        for (int i = 0; i < this.weapon.getBullets().size(); i++) {
            Rect bullet = this.weapon.getBullets().get(i);

            if (bullet.left > screenWidth) {
                this.weapon.getBullets().remove(bullet);
            }

        }
        for (int i = 0; i < this.weapon.getBullets().size(); i++) {
            Rect bullet = this.weapon.getBullets().get(i);
            if (this.enemy.getEnemyHitbox().intersect(bullet)) {
                this.weapon.getBullets().remove(i);

                    this.enemy.setGotHit(this.enemy.getGotHit() + 1);
                    this.life = life -1;
                    this.score = StaticClass.messageClass.score2 + 10;
                    if(life <= 0){
                        this.gamefinished();
                    }

            }

        }
        if (loop % enemyMoveTime == 0)
        {
            Random random = new Random();
            int r = random.nextInt(3);

                if (r == 0 && this.enemy.getxPos() > 500)
                {
                    Log.d("x","0");

                    this.enemy.setxPos(this.enemy.getxPos() - 150);
                    this.enemy.updateHitbox();
                    if(this.enemy.getxPos()>screenWidth){
                        enemy.setxPos(this.enemy.getxPos() + 500);
                        this.enemy.updateHitbox();
                    }
                }
                else if (r == 1 && this.enemy.getxPos() < screenWidth)
                {
                    Log.d("x","1");
                    this.enemy.setxPos(this.enemy.getxPos() + 20);
                    this.enemy.updateHitbox();
                    if(this.enemy.getxPos()>screenWidth){
                        enemy.setxPos(this.enemy.getxPos() - 500);
                        this.enemy.updateHitbox();
                    }
                }
                else if (r == 2 && this.enemy.getyPos() < screenHeight)
                {
                    Log.d("x","2");
                    this.enemy.setyPos(this.enemy.getyPos() + 20);
                    this.enemy.updateHitbox();

                }
                else if(r == 3 && this.enemy.getyPos() > 500)
                {
                    Log.d("x","3");
                    this.enemy.setyPos(this.enemy.getyPos() );
                    this.enemy.updateHitbox();
                    if(this.enemy.getyPos()>screenHeight){
                        enemy.setyPos(this.enemy.getyPos() + 500);
                        this.enemy.updateHitbox();
                    }
                }
                else {
                    Log.d("x","nil");
                    this.enemy.setxPos(this.enemy.getxPos() - 20);
                    this.enemy.setyPos(this.enemy.getyPos() + 20);
                    this.enemy.updateHitbox();
                }

        }

    }

    private void gamefinished() {
        Intent i = new Intent(this.getContext(),gameOver.class);
        this.getContext().startActivity(i);
    }


    void spawnPlayerBullet() {
        Rect bullet = new Rect(this.weapon.getxPos(),
                this.weapon.getyPos() + this.weapon.getWeaponImage().getHeight() / 2,
                this.weapon.getxPos() + 50,
                this.weapon.getyPos() + this.weapon.getWeaponImage().getHeight() / 2 + 25
        );
        this.weapon.getBullets().add(bullet);

    }


    @Override
    public void run() {
        while (gameIsRunning == true) {
            this.updatePositions();
            this.redrawSprites();
            this.setFPS();
        }
    }

    public void setFPS() {
        try {
            gameThread.sleep(60);
        } catch (Exception e) {

        }
    }

    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {

        }
    }

    public void startGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?
        if (userAction == MotionEvent.ACTION_DOWN)
        {


            int middleOfScreen = this.screenHeight/ 2;
            if (event.getY() <= middleOfScreen)
            {

//                weapon.setxPos(weapon.getxPos() - weapon.getSpeed());
                weapon.setyPos(weapon.getyPos() - weapon.getSpeed());
                this.weapon.updateHitbox();
            }
            else if (event.getY() > middleOfScreen)
            {

//                weapon.setxPos(weapon.getxPos() + weapon.getSpeed());
                weapon.setyPos(weapon.getyPos() + weapon.getSpeed());
                this.weapon.updateHitbox();
            }
        }
        else if (userAction == MotionEvent.ACTION_UP)
        {
            spawnPlayerBullet();
        }
        return true;
    }

}



